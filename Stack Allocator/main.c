#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <stdint.h>
#include <windows.h>
#include <windowsx.h>
#include <tlhelp32.h>
#include <Shlobj.h>
#include <direct.h>
#include <stdbool.h>
#include <tchar.h>
#include <strsafe.h>
#include <fileapi.h>
#include <math.h>
#include <time.h>
#include <assert.h>
#include <inttypes.h>

// allocator based on https://www.gingerbill.org/article/2019/02/08/memory-allocation-strategies-002/
// love u bill <3

#define function static
#ifndef DEFAULT_ALIGNMENT
#define DEFAULT_ALIGNMENT (2*sizeof(void *))
#endif

#define Bytes(n)      (n)
#define Kilobytes(n)  (n << 10)
#define Megabytes(n)  (n << 20)
#define Gigabytes(n)  (((U64)n) << 30)
#define Terabytes(n)  (((U64)n) << 40)

#define Thousand(n) ((n)*1000)
#define Million(n)  ((n)*1000000)
#define Billion(n)  ((n)*1000000000LL)

#define ARENA_COMMIT_GRANULARITY Kilobytes(4)
#define ARENA_DECOMMIT_THRESHOLD Megabytes(64)

#define Swap(type, a, b) do{ type _swapper_ = a; a = b; b = _swapper_; }while(0)
#define Min(a, b) (((a)<(b)) ? (a) : (b))
#define Max(a, b) (((a)>(b)) ? (a) : (b))

typedef uint8_t U8;
typedef uint64_t U64;
typedef uintptr_t UPTR;

function U64
OS_PageSize (void)
{
  SYSTEM_INFO info;
  GetSystemInfo (&info);
  return info.dwPageSize;
}

function void *
OS_Reserve (U64 size)
{
  U64 gb_snapped_size = size;
  gb_snapped_size += Gigabytes (1) - 1;
  gb_snapped_size -= gb_snapped_size % Gigabytes (1);
  void *ptr = VirtualAlloc (0, gb_snapped_size, MEM_RESERVE, PAGE_NOACCESS);
  return ptr;
}

function void
OS_Release (void *ptr, U64 size)
{
  VirtualFree (ptr, 0, MEM_RELEASE);
}

function void
OS_Commit (void *ptr, U64 size)
{
  U64 page_snapped_size = size;
  page_snapped_size += OS_PageSize () - 1;
  page_snapped_size -= page_snapped_size % OS_PageSize ();
  VirtualAlloc (ptr, page_snapped_size, MEM_COMMIT, PAGE_READWRITE);
}

function void
OS_Decommit (void *ptr, U64 size)
{
  VirtualFree (ptr, size, MEM_DECOMMIT);
}


typedef struct StackAllocator StackAllocator;
struct StackAllocator
{
  U64 *buffer;
  U64 remaining;
  U64 used;
  U64 prevOffset;
  U64 offset;
};


// if we need a higher alignment then we can increase the size of the padding
// use this equation 2^(8*(padding type i.e. U8, U64, etc.) - 1)
typedef struct StackAllocatorHeader StackAllocatorHeader;
struct StackAllocatorHeader
{
  U64 prevOffset;
  U64 padding;
};

function bool
is_power_of_two (uintptr_t x)
{
  return (x & (x - 1)) == 0;
}

U64
calc_padding_with_header (UPTR ptr, UPTR alignment, U64 headerSize)
{
  UPTR p, a, mod, padding, spaceNeeded;

  assert (is_power_of_two (alignment));

  p = ptr;
  a = alignment;
  mod = p & (a - 1);		// p % a
  padding = 0;
  spaceNeeded = 0;

  if (mod != 0)
  {
    padding = a - mod;
  }

  spaceNeeded = (UPTR) headerSize;

  if (padding < spaceNeeded)
  {
    spaceNeeded -= padding;

    if ((spaceNeeded & (a - 1)) != 0)
    {
      padding += a * (1 + (spaceNeeded / a));
    } else
    {
      padding += a * (spaceNeeded / a);
    }
  }

  return (U64) padding;
}

function void *
stack_alloc_align (StackAllocator * s, U64 size, U64 alignment)
{
  UPTR currAddr, nextAddr;
  U64 padding;
  StackAllocatorHeader *header;

  assert (is_power_of_two (alignment));

  if (alignment > 128)
  {
    // GBill: As the padding is 8 bits (1 byte), the largest alignment that can be used is 128 bytes
    // Lake: i assume this can be updated later?
    alignment = 128;
  }

  currAddr = (UPTR) s->buffer + (UPTR) s->offset;
  padding =
    calc_padding_with_header (currAddr, (UPTR) alignment,
			      (U64) sizeof (StackAllocatorHeader));

  if (s->offset + padding + size > s->remaining)
  {
    // no more memory for yooooooooouuuuuuu
    return NULL;
  }
  s->prevOffset = s->offset;
  s->offset += padding;

  nextAddr = currAddr + (UPTR) padding;
  header =
    (StackAllocatorHeader *) (nextAddr - (U64) sizeof (StackAllocatorHeader));
  header->padding = (UPTR) padding;
  header->prevOffset = s->prevOffset;

  s->offset += size;
  s->remaining -= size;
  s->used += size;

  return memset ((void *) nextAddr, 0, (size_t) size);
}

// juice
function void
stack_alloc_init (StackAllocator * s,
		  void *backingBuffer, U64 backingBufferLength)
{
  s->buffer = (U64 *) backingBuffer;
  s->remaining = backingBufferLength;
  s->used = 0;
  s->offset = 0;
}

function void *
stack_alloc (StackAllocator * s, U64 size)
{
  return stack_alloc_align (s, size, DEFAULT_ALIGNMENT);
}

#define S_Alloc(s, t) (t *)stack_alloc((s), (U64)sizeof(t))

function void
stack_free (StackAllocator * s, void *ptr)
{
  if (ptr == NULL)
  {
    return;
  } else
  {
    UPTR start, end, currAddr;
    StackAllocatorHeader *header;
    U64 prevOffset;

    start = (UPTR) s->buffer;
    end = start + (UPTR) s->remaining;
    currAddr = (UPTR) ptr;

    if (!(start <= currAddr && currAddr < end))
    {
      assert (0
	      &&
	      "out of bounds memory address passed to allocator you idiot.");
      return;
    }

    if (currAddr >= start + (UPTR) s->offset)
    {
      // allow double frees ??
      return;
    }

    header =
      (StackAllocatorHeader *) (currAddr -
				(U64) sizeof (StackAllocatorHeader));
    prevOffset = currAddr - (UPTR) header->padding - start;

    if (prevOffset != header->prevOffset)
    {
      assert (0 && "Out of order stack allocator free");
      return;
    }
    //memset((void *) currAddr, 0, (size_t)());

    s->remaining += s->used;
    s->used = s->prevOffset - s->offset;
    s->offset = s->prevOffset;
    s->prevOffset = header->prevOffset;
  }
}

function void *
stack_resize_align (StackAllocator * s,
		    void *ptr, U64 oldSize, U64 newSize, U64 alignment)
{
  if (ptr == NULL)
  {
    return stack_alloc_align (s, newSize, alignment);
  } else if (newSize == 0)
  {
    stack_free (s, ptr);
    return NULL;
  } else
  {
    UPTR start, end, currAddr;
    U64 minSize = oldSize < newSize ? oldSize : newSize;
    void *newPtr;

    start = (UPTR) s->buffer;
    end = start + (UPTR) s->remaining;
    currAddr = (UPTR) ptr;

    if (!(start <= currAddr && currAddr < end))
    {
      assert (0
	      &&
	      "out of bounds memory address passed to allocator you idiot.");
      return NULL;
    }
    if (currAddr >= start + (UPTR) s->offset)
    {
      // allow double frees ??
      return NULL;
    }
    if (oldSize == newSize)
    {
      return ptr;
    }

    newPtr = stack_alloc_align (s, newSize, alignment);
    memmove (newPtr, ptr, minSize);
    return newPtr;
  }
}

function void *
stack_resize (StackAllocator * s, void *ptr, U64 oldSize, U64 newSize)
{
  return stack_resize_align (s, ptr, oldSize, newSize, DEFAULT_ALIGNMENT);
}

typedef struct SomeData SomeData;
struct SomeData
{
  int i;
  U8 *string;
};

int
main (void)
{
  U64 buff[1024];
  StackAllocator s = { 0 };
  stack_alloc_init (&s, buff, 1024);

  StackAllocator *sp = &s;

  if (sp->buffer)
  {
    printf ("we got a memory buffer yall.\n");
    SomeData *d1 = S_Alloc (sp, SomeData);
    d1->i = 8173123;
    d1->string = "Poo poo pee pee";
    printf ("remaining: %" PRIu64 "\n", (sp->remaining));
    printf ("d1-> %d\n", d1->i);
    printf ("d1-> %s\n", d1->string);
    stack_free (sp, d1);
    printf ("remaining: %" PRIu64 "\n", (sp->remaining));
    printf ("d1-> %d\n", d1->i);
    printf ("d1-> %s\n", d1->string);
  }

  return 0;
}
