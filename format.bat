@ECHO off

REM This script is used to format code files and clean the temps that get generated from running the indent program

REM -------------------------------------
REM Variables
REM -------------------------------------

REM indent doesn't like when u pass variables i guess

REM -------------------------------------
REM Main
REM -------------------------------------

CALL :FormatterCheck
CALL :FormatAll
CALL :Clean
GOTO :EOF

REM -------------------------------------
REM Functions
REM -------------------------------------

REM Function to check for formatting program
:FormatterCheck
WHERE /q indent
IF ERRORLEVEL 1 (
	ECHO Program: indent.exe not found.
	ECHO Program: indent.exe indent can be downloaded here: http://gnuwin32.sourceforge.net/packages/indent.html
	ECHO You will need to run the setup and make sure the program is in your PATH
) ELSE (
	ECHO Program: indent.exe exists. Let's go!
	ECHO Formatting code.
)
GOTO :EOF


REM Run formatter
:FormatAll
FOR %%f IN (*.c) DO indent -bl -pcs -cs -bs -saf -sai -saw -ce -bli0 -cli2 -cbi0 -blf -ip2 -sai -saw -saf %%f
FOR /D %%d IN (*) DO (
    PUSHD %%d >nul
    CALL :FormatAll
    POPD >nul
)

FOR %%f IN (*.h) DO indent %%f
FOR /D %%d IN (*) DO (
    PUSHD %%d >nul
    CALL :FormatAll
    POPD >nul
)
GOTO :EOF

REM Removes all temp generated files from calling the formatter
:Clean
DEL /Q /F /S "*~" > NUL
GOTO :EOF