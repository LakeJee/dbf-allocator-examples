/* date = April 24th 2023 2:10 pm */

#ifndef _LINEAR_ALLOCATOR_H
#define _LINEAR_ALLOCATOR_H

/* 
 * A simple memory arena designed for Win32 applications.
*
* It will be a linear allocator, that can allocate and deallocate in order 
* of the allocations that have already been made.
* 
* Allocator (Change later?)
 * void    *base: Pointer to buffer where we make allocations.
 * U64      next: Number that represents the next valid *location* in the arena
*                where an allocation can be made.
* U64       end: Number that represents the last valid *lolcation* in the arena
          *                where an allocation can be made.
* U64      used: Number that represents *total memory* used by the arena.
* U64 totalSize: Number that represents *total size* of the arena.
* 
*/
typedef struct Allocator Allocator;
struct Allocator
{
  void *base;
  HANDLE handle;
  U64 next;
  U64 end;
  U64 used;
  U64 totalSize;
};

typedef struct TempAllocator TempAllocator;
struct TempAllocator
{
  Allocator *a;
  U64 used;
};


function Allocator * allocator_init_default ();
function Allocator * allocator_init (U64 size);
function void * allocator_alloc_aligned (Allocator * a, U64 size);
function void allocator_rollback (Allocator * a, U64 size);
function bool allocator_free (Allocator * a);


function TempAllocator allocator_temp_create (Allocator * a);
function void * allocator_temp_allocate (TempAllocator * t, U64 size);
function void allocator_temp_release (TempAllocator t);
function U64 allocator_get_remaining (Allocator * a);


/*
* --------------------------------------
* General purpose Allocator API
* --------------------------------------
*
*/
// Initialization
#define AllocInitDefault() allocator_init_default()
#define AllocInit(size) allocator_init(size)
// Allocate/deallocate based on type
#define Alloc(a, type, count) (type *) allocator_alloc_aligned(a, (U64)(sizeof(type)) * (count))
#define Dealloc(a, type, count) allocator_rollback(a, (U64)(sizeof(type)) * (count))
// Raw size (in bytes) allocation/dallocation
#define AllocRawSize(a, size) allocator_alloc_aligned(a, (U64)size)
#define DeallocRawSize(a, size) allocator_rollback(a, (U64)size)
// Free the allocator when we're done with it
#define AllocatorFree(a) allocator_free(a)


/*
* --------------------------------------
* Utilities for Allocator API
* --------------------------------------
*
*/
// Get remaining amount of memory
#define GetRemaining(a) allocator_get_remaining(a)
// Create temp allocator
#define GetTemp(a) allocator_temp_create(a)
// Allocate to the temp allocator
#define TempAlloc(t, type, count) allocator_temp_allocate(&t, (U64)(sizeof(type)) * (count))
#define TempAllocRawSize(t, size) AllocRawSize(t.a, size)
// Release temp allocator
#define ReleaseTemp(t) allocator_temp_release(t)


#endif //_LINEAR_ALLOCATOR_H
