#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <stdint.h>
#include <windows.h>
#include <windowsx.h>
#include <tlhelp32.h>
#include <Shlobj.h>
#include <direct.h>
#include <stdbool.h>
#include <tchar.h>
#include <strsafe.h>
#include <fileapi.h>
#include <math.h>
#include <time.h>
#include <assert.h>
#include <inttypes.h>

#define function   static
#define MemoryCopy memcpy
#define MemorySet  memset

typedef uint8_t U8;
typedef uint64_t U64;
typedef uintptr_t UPTR;

#ifndef DEFAULT_ALIGNMENT
#define DEFAULT_ALIGNMENT (2*sizeof(void *))
#endif

#define ALIGN(size) (((size) + (DEFAULT_ALIGNMENT - 1)) & ~(DEFAULT_ALIGNMENT - 1))
#define POWER_OF_TWO(x)  (((x) & ((x) - 1)) == 0)

#define Bytes(n)      (n)
#define Kilobytes(n)  (n << 10)
#define Megabytes(n)  (n << 20)
#define Gigabytes(n)  (((U64)n) << 30)
#define Terabytes(n)  (((U64)n) << 40)

#define Thousand(n) ((n)*1000)
#define Million(n)  ((n)*1000000)
#define Billion(n)  ((n)*1000000000LL)

#define Swap(type, a, b) do{ type _swapper_ = a; a = b; b = _swapper_; }while(0)
#define Min(a, b) (((a)<(b)) ? (a) : (b))
#define Max(a, b) (((a)>(b)) ? (a) : (b))


// LINEAR ALLOCATOR API
#include "LinearAllocator.h"
#include "LinearAllocator.c"


/* 
* --------------------------------------
 * Testing
 * --------------------------------------
 * We can use this array concat to test our allocator. 
 * Stolen from https://rosettacode.org/wiki/Array_concatenation#C
* 
*/
function void *
array_concat (Allocator * allocator, const void *array1, size_t count1, const void *array2,
              size_t count2, size_t size)
{
  U8 *p = AllocRawSize (allocator, (size * (count1 + count2)));	// allocation will go here to replace: malloc (size * (count1 + count2));
  MemoryCopy (p, array1, count1 * size);
  MemoryCopy (p + count1 * size, array2, count2 * size);
  return p;
}

#define ARRAY_CONCAT(allocator, TYPE, A, An, B, Bn) \
(TYPE *)array_concat(allocator, (const void *)(A), (An), (const void *)(B), (Bn), sizeof(TYPE));

typedef struct SomeData SomeData;
struct SomeData
{
  U8 *nameOfData;
  U64 actualData;
};

int
main (void)
{
  Allocator *a = AllocInit (Gigabytes (2));
  
  if (a->totalSize)
  {
    /* 
     * This will run infinitely.
     *
     * Open task manager or whatever you use to monitor system usage and see if it is running smoothly.
     * Smoothly meaning CPU/Memory usage IS NOT climbing to infinity while main.exe is running lol.
     *
     * Usage ~should~ be low and not climb at all.
     * 
     * To test the AllocatorFree function, comment out the while loop to ensure it is freeing properly.
     */
    while (1)
    {
      // test array concat
      U8 arr1[] = { 1, 2, 3, 4, 5 };
      U8 arr2[] = { 6, 7, 8, 9, 0 };
      U8 *concatResult = ARRAY_CONCAT (a, U8, arr1, 5, arr2, 5);
      if (concatResult)
      {
        printf ("--------------begin--------------\n");
        printf ("total size of a :              %" PRIu64 "\n", a->totalSize);
        printf ("amount of memory used from a : %" PRIu64 "\n", a->used);
        printf ("array data arr1 pre concat [");
        for (int i = 0; i < 5; i++)
        {
          printf ("%d ", arr1[i]);
        }
        printf ("]\n");
        printf ("array data arr2 pre concat [");
        for (int i = 0; i < 5; i++)
        {
          printf ("%d ", arr2[i]);
        }
        printf ("]\n");
        printf ("array data post concat [");
        for (int i = 0; i < 10; i++)
        {
          if (i == 9)
            printf ("%d", concatResult[i]);
          else
            printf ("%d ", concatResult[i]);
        }
        printf ("]\n");
      }
      
      DeallocRawSize (a, 10);
      if (concatResult[0] == 0 &&
          concatResult[1] == 0 &&
          concatResult[2] == 0 &&
          concatResult[3] == 0 &&
          concatResult[4] == 0 &&
          concatResult[5] == 0 &&
          concatResult[6] == 0 &&
          concatResult[7] == 0 &&
          concatResult[8] == 0 && concatResult[9] == 0)
      {
        printf ("**IF YOU SEE THIS MESSAGE THEN THE DEALLOC WORKED!**\n");
      }
      printf ("total size of a :              %" PRIu64 "\n", a->totalSize);
      printf ("amount of memory used from a : %" PRIu64 "\n", a->used);
      printf ("--------------end--------------\n");
      
      printf ("\n\n");
      
      // test structs
      SomeData *d1 = Alloc (a, SomeData, 1);
      SomeData *d2 = Alloc (a, SomeData, 1);
      SomeData *d3 = Alloc (a, SomeData, 1);
      SomeData *d4 = Alloc (a, SomeData, 1);
      SomeData *d5 = Alloc (a, SomeData, 1);
      SomeData *d6 = Alloc (a, SomeData, 1);
      SomeData *d7 = Alloc (a, SomeData, 1);
      SomeData *d8 = Alloc (a, SomeData, 1);
      SomeData *d9 = Alloc (a, SomeData, 1);
      SomeData *d10 = Alloc (a, SomeData, 1);
      SomeData *d11 = Alloc (a, SomeData, 1);
      SomeData *d12 = Alloc (a, SomeData, 1);
      SomeData *d13 = Alloc (a, SomeData, 1);
      SomeData *d14 = Alloc (a, SomeData, 1);
      SomeData *d15 = Alloc (a, SomeData, 1);
      SomeData *d16 = Alloc (a, SomeData, 1);
      SomeData *d17 = Alloc (a, SomeData, 1);
      SomeData *d18 = Alloc (a, SomeData, 1);
      SomeData *d19 = Alloc (a, SomeData, 1);
      SomeData *d20 = Alloc (a, SomeData, 1);
      SomeData *d21 = Alloc (a, SomeData, 1);
      SomeData *d22 = Alloc (a, SomeData, 1);
      SomeData *d23 = Alloc (a, SomeData, 1);
      SomeData *d24 = Alloc (a, SomeData, 1);
      SomeData *d25 = Alloc (a, SomeData, 1);
      if (d1 && d2 && d3 && d4 && d5 &&
          d6 && d7 && d8 && d9 && d10 &&
          d11 && d12 && d13 && d14 && d15 &&
          d16 && d17 && d18 && d19 && d20 && d21 && d22 && d23 && d24 && d25)
      {
        d1->nameOfData = "d1";
        d1->actualData = 152329;
        d2->nameOfData = "d2";
        d2->actualData = 1234628;
        d3->nameOfData = "d3";
        d3->actualData = 345235;
        d4->nameOfData = "d4";
        d4->actualData = 253413;
        d5->nameOfData = "d5";
        d5->actualData = 7856781;
        d6->nameOfData = "d6";
        d6->actualData = 152329;
        d7->nameOfData = "d7";
        d7->actualData = 1234628;
        d8->nameOfData = "d8";
        d8->actualData = 345235;
        d9->nameOfData = "d9";
        d9->actualData = 253413;
        d10->nameOfData = "d10";
        d10->actualData = 7856781;
        d11->nameOfData = "d11";
        d11->actualData = 152329;
        d12->nameOfData = "d12";
        d12->actualData = 1234628;
        d13->nameOfData = "d13";
        d13->actualData = 345235;
        d14->nameOfData = "d14";
        d14->actualData = 253413;
        d15->nameOfData = "d15";
        d15->actualData = 7856781;
        d16->nameOfData = "d16";
        d16->actualData = 152329;
        d17->nameOfData = "d17";
        d17->actualData = 1234628;
        d18->nameOfData = "d18";
        d18->actualData = 345235;
        d19->nameOfData = "d19";
        d19->actualData = 253413;
        d20->nameOfData = "d20";
        d20->actualData = 7856781;
        d21->nameOfData = "d21";
        d21->actualData = 152329;
        d22->nameOfData = "d22";
        d22->actualData = 1234628;
        d23->nameOfData = "d23";
        d23->actualData = 345235;
        d24->nameOfData = "d24";
        d24->actualData = 253413;
        d25->nameOfData = "d25";
        d25->actualData = 7856781;
        
        
        printf ("--------------begin--------------\n");
        printf ("total size of a :              %" PRIu64 "\n", a->totalSize);
        printf ("amount of memory used from a : %" PRIu64 "\n", a->used);
        printf ("the name of data1: %s.\n", d1->nameOfData);
        printf ("the data of data1: %lld.\n", (long long) d1->actualData);
        printf ("the name of data2: %s.\n", d2->nameOfData);
        printf ("the data of data2: %lld.\n", (long long) d2->actualData);
        printf ("the name of data3: %s.\n", d3->nameOfData);
        printf ("the data of data3: %lld.\n", (long long) d3->actualData);
        printf ("the name of data4: %s.\n", d4->nameOfData);
        printf ("the data of data4: %lld.\n", (long long) d4->actualData);
        printf ("the name of data5: %s.\n", d5->nameOfData);
        printf ("the data of data5: %lld.\n", (long long) d5->actualData);
        printf ("the name of data6: %s.\n", d6->nameOfData);
        printf ("the data of data6: %lld.\n", (long long) d6->actualData);
        printf ("the name of data7: %s.\n", d7->nameOfData);
        printf ("the data of data7: %lld.\n", (long long) d7->actualData);
        printf ("the name of data8: %s.\n", d8->nameOfData);
        printf ("the data of data8: %lld.\n", (long long) d8->actualData);
        printf ("the name of data9: %s.\n", d9->nameOfData);
        printf ("the data of data9: %lld.\n", (long long) d9->actualData);
        printf ("the name of data10: %s.\n", d10->nameOfData);
        printf ("the data of data10: %lld.\n", (long long) d10->actualData);
        printf ("the name of data11: %s.\n", d11->nameOfData);
        printf ("the data of data11: %lld.\n", (long long) d11->actualData);
        printf ("the name of data12: %s.\n", d12->nameOfData);
        printf ("the data of data12: %lld.\n", (long long) d12->actualData);
        printf ("the name of data13: %s.\n", d13->nameOfData);
        printf ("the data of data13: %lld.\n", (long long) d13->actualData);
        printf ("the name of data14: %s.\n", d14->nameOfData);
        printf ("the data of data14: %lld.\n", (long long) d14->actualData);
        printf ("the name of data15: %s.\n", d15->nameOfData);
        printf ("the data of data15: %lld.\n", (long long) d15->actualData);
        printf ("the name of data16: %s.\n", d16->nameOfData);
        printf ("the data of data16: %lld.\n", (long long) d16->actualData);
        printf ("the name of data17: %s.\n", d17->nameOfData);
        printf ("the data of data17: %lld.\n", (long long) d17->actualData);
        printf ("the name of data18: %s.\n", d18->nameOfData);
        printf ("the data of data18: %lld.\n", (long long) d18->actualData);
        printf ("the name of data19: %s.\n", d19->nameOfData);
        printf ("the data of data19: %lld.\n", (long long) d19->actualData);
        printf ("the name of data20: %s.\n", d20->nameOfData);
        printf ("the data of data20: %lld.\n", (long long) d20->actualData);
        printf ("the name of data21: %s.\n", d21->nameOfData);
        printf ("the data of data21: %lld.\n", (long long) d21->actualData);
        printf ("the name of data22: %s.\n", d22->nameOfData);
        printf ("the data of data22: %lld.\n", (long long) d22->actualData);
        printf ("the name of data23: %s.\n", d23->nameOfData);
        printf ("the data of data23: %lld.\n", (long long) d23->actualData);
        printf ("the name of data24: %s.\n", d24->nameOfData);
        printf ("the data of data24: %lld.\n", (long long) d24->actualData);
        printf ("the name of data25: %s.\n", d25->nameOfData);
        printf ("the data of data25: %lld.\n", (long long) d25->actualData);
      }
      
      Dealloc (a, SomeData, 25);
      printf ("** EVERY VALUE BELOW HERE SHOULD NULL AND 0 **\n");
      printf ("the name of data1: %s.\n", d1->nameOfData);
      printf ("the data of data1: %lld.\n", (long long) d1->actualData);
      printf ("the name of data2: %s.\n", d2->nameOfData);
      printf ("the data of data2: %lld.\n", (long long) d2->actualData);
      printf ("the name of data3: %s.\n", d3->nameOfData);
      printf ("the data of data3: %lld.\n", (long long) d3->actualData);
      printf ("the name of data4: %s.\n", d4->nameOfData);
      printf ("the data of data4: %lld.\n", (long long) d4->actualData);
      printf ("the name of data5: %s.\n", d5->nameOfData);
      printf ("the data of data5: %lld.\n", (long long) d5->actualData);
      printf ("the name of data6: %s.\n", d6->nameOfData);
      printf ("the data of data6: %lld.\n", (long long) d6->actualData);
      printf ("the name of data7: %s.\n", d7->nameOfData);
      printf ("the data of data7: %lld.\n", (long long) d7->actualData);
      printf ("the name of data8: %s.\n", d8->nameOfData);
      printf ("the data of data8: %lld.\n", (long long) d8->actualData);
      printf ("the name of data9: %s.\n", d9->nameOfData);
      printf ("the data of data9: %lld.\n", (long long) d9->actualData);
      printf ("the name of data10: %s.\n", d10->nameOfData);
      printf ("the data of data10: %lld.\n", (long long) d10->actualData);
      printf ("the name of data11: %s.\n", d11->nameOfData);
      printf ("the data of data11: %lld.\n", (long long) d11->actualData);
      printf ("the name of data12: %s.\n", d12->nameOfData);
      printf ("the data of data12: %lld.\n", (long long) d12->actualData);
      printf ("the name of data13: %s.\n", d13->nameOfData);
      printf ("the data of data13: %lld.\n", (long long) d13->actualData);
      printf ("the name of data14: %s.\n", d14->nameOfData);
      printf ("the data of data14: %lld.\n", (long long) d14->actualData);
      printf ("the name of data15: %s.\n", d15->nameOfData);
      printf ("the data of data15: %lld.\n", (long long) d15->actualData);
      printf ("the name of data16: %s.\n", d16->nameOfData);
      printf ("the data of data16: %lld.\n", (long long) d16->actualData);
      printf ("the name of data17: %s.\n", d17->nameOfData);
      printf ("the data of data17: %lld.\n", (long long) d17->actualData);
      printf ("the name of data18: %s.\n", d18->nameOfData);
      printf ("the data of data18: %lld.\n", (long long) d18->actualData);
      printf ("the name of data19: %s.\n", d19->nameOfData);
      printf ("the data of data19: %lld.\n", (long long) d19->actualData);
      printf ("the name of data20: %s.\n", d20->nameOfData);
      printf ("the data of data20: %lld.\n", (long long) d20->actualData);
      printf ("the name of data21: %s.\n", d21->nameOfData);
      printf ("the data of data21: %lld.\n", (long long) d21->actualData);
      printf ("the name of data22: %s.\n", d22->nameOfData);
      printf ("the data of data22: %lld.\n", (long long) d22->actualData);
      printf ("the name of data23: %s.\n", d23->nameOfData);
      printf ("the data of data23: %lld.\n", (long long) d23->actualData);
      printf ("the name of data24: %s.\n", d24->nameOfData);
      printf ("the data of data24: %lld.\n", (long long) d24->actualData);
      printf ("the name of data25: %s.\n", d25->nameOfData);
      printf ("the data of data25: %lld.\n", (long long) d25->actualData);
      printf
      ("used of a should be smaller than the above used amount : %"
       PRIu64 "\n", a->used);
      
      
      // temp allocator test
      TempAllocator t = GetTemp (a);
      SomeData *d26 = TempAlloc (t, SomeData, 1);
      SomeData *d27 = TempAlloc (t, SomeData, 1);
      SomeData *d28 = TempAlloc (t, SomeData, 1);
      SomeData *d29 = TempAlloc (t, SomeData, 1);
      SomeData *d30 = TempAlloc (t, SomeData, 1);
      
      if (d26 && d27 && d28 && d29 && d30)
      {
        printf ("we got ourselves a temp allocator yall.\n");
        printf ("amount of memory used from a : %" PRIu64 "\n", a->used);
        d26->nameOfData = "d26";
        d26->actualData = 152329;
        d27->nameOfData = "d27";
        d27->actualData = 1234628;
        d28->nameOfData = "d28";
        d28->actualData = 345235;
        d29->nameOfData = "d29";
        d29->actualData = 253413;
        d30->nameOfData = "d30";
        d30->actualData = 7856781;
        printf ("the name of data26: %s.\n", d26->nameOfData);
        printf ("the data of data26: %lld.\n", (long long) d26->actualData);
        printf ("the name of data27: %s.\n", d27->nameOfData);
        printf ("the data of data27: %lld.\n", (long long) d27->actualData);
        printf ("the name of data28: %s.\n", d28->nameOfData);
        printf ("the data of data28: %lld.\n", (long long) d28->actualData);
        printf ("the name of data29: %s.\n", d29->nameOfData);
        printf ("the data of data29: %lld.\n", (long long) d29->actualData);
        printf ("the name of data30: %s.\n", d30->nameOfData);
        printf ("the data of data30: %lld.\n", (long long) d30->actualData);
        
        ReleaseTemp (t);
        printf ("amount of memory used from a : %" PRIu64 "\n", a->used);
      }
      
      printf ("--------------end--------------\n");
    }
    
    if (AllocatorFree (a))
    {
      printf ("We done see ya!\n");
    } else
    {
      printf ("Uh-oh failed to free the allocator oopsie tee hee :)\n");
    }
    
  }
  return 0;
}
