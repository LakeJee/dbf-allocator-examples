#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <stdint.h>
#include <windows.h>
#include <windowsx.h>
#include <tlhelp32.h>
#include <Shlobj.h>
#include <direct.h>
#include <stdbool.h>
#include <tchar.h>
#include <strsafe.h>
#include <fileapi.h>
#include <math.h>
#include <time.h>
#include <assert.h>
#include <inttypes.h>

// allocator based on https://www.gingerbill.org/article/2021/11/30/memory-allocation-strategies-005/

#define function static
#define MemoryCopy memcpy
#define MemorySet  memset

typedef uint8_t U8;
typedef uint64_t U64;
typedef uintptr_t UPTR;

#ifndef DEFAULT_ALIGNMENT
#define DEFAULT_ALIGNMENT (2*sizeof(void *))
#endif

#define Bytes(n)      (n)
#define Kilobytes(n)  (n << 10)
#define Megabytes(n)  (n << 20)
#define Gigabytes(n)  (((U64)n) << 30)
#define Terabytes(n)  (((U64)n) << 40)

#define Thousand(n) ((n)*1000)
#define Million(n)  ((n)*1000000)
#define Billion(n)  ((n)*1000000000LL)

#define ARENA_COMMIT_GRANULARITY Kilobytes(4)
#define ARENA_DECOMMIT_THRESHOLD Megabytes(64)

#define Swap(type, a, b) do{ type _swapper_ = a; a = b; b = _swapper_; }while(0)
#define Min(a, b) (((a)<(b)) ? (a) : (b))
#define Max(a, b) (((a)>(b)) ? (a) : (b))


#define ALIGN(size) (((size) + (DEFAULT_ALIGNMENT - 1)) & ~(DEFAULT_ALIGNMENT - 1))
#define POWER_OF_TWO(x)  (((x) & ((x) - 1)) == 0)

#include "DynamicAllocator.h"
#include "DynamicAllocator.c"

typedef struct SomeData SomeData;
struct SomeData
{
  char *nameOfData;
  int actualData;
};

int
main ()
{
  // this will defo have to be reworked but it works for now
  Tree  tree = init_tree();
  
  Pool *p = pool_init(&tree, Gigabytes(1));
  
  if (p)
  {
    printf ("pool size -> %" PRIu64 "\n", p->_size);
    printf ("pool used -> %" PRIu64 "\n", p->used);
    printf ("pool remaining -> %" PRIu64 "\n", p->remaining);
    printf ("holy duck fuck.\n");
    
    SomeData *d1 = pool_alloc (p, sizeof(SomeData), DEFAULT_ALIGNMENT);
    SomeData *d2 = pool_alloc (p, sizeof(SomeData), DEFAULT_ALIGNMENT);
    SomeData *d3 = pool_alloc (p, sizeof(SomeData), DEFAULT_ALIGNMENT);
    //SomeData *d4 = pool_alloc (p, sizeof(SomeData), DEFAULT_ALIGNMENT);
    //SomeData *d5 = pool_alloc (p, sizeof(SomeData), DEFAULT_ALIGNMENT);
    
    if (d1 && d2 && d3)
    {
      printf ("i am the fucking goat\n");
      
      d1->nameOfData = "d1";
      d1->actualData = 152329;
      d2->nameOfData = "d2";
      d2->actualData = 1234628;
      d3->nameOfData = "d3";
      d3->actualData = 345235;
      
      
      
      printf ("the name of data1: %s.\n", d1->nameOfData);
      printf ("the data of data1: %d.\n", d1->actualData);
      printf ("the name of data2: %s.\n", d2->nameOfData);
      printf ("the data of data2: %d.\n", d2->actualData);
      printf ("the name of data3: %s.\n", d3->nameOfData);
      printf ("the data of data3: %d.\n", d3->actualData);
      
      
      printf ("pool size -> %" PRIu64 "\n", p->_size);
      printf ("pool used -> %" PRIu64 "\n", p->used);
      printf ("pool remaining -> %" PRIu64 "\n", p->remaining);
    }
    
  }
  return 0;
}
