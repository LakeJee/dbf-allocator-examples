#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <stdint.h>
#include <windows.h>
#include <windowsx.h>
#include <tlhelp32.h>
#include <Shlobj.h>
#include <direct.h>
#include <stdbool.h>
#include <tchar.h>
#include <strsafe.h>
#include <fileapi.h>
#include <math.h>
#include <time.h>
#include <assert.h>
#include <inttypes.h>

// allocator based on https://www.gingerbill.org/article/2021/11/30/memory-allocation-strategies-005/

#define function static
#define MemoryCopy memcpy
#define MemorySet  memset

typedef uint8_t U8;
typedef uint64_t U64;
typedef uintptr_t UPTR;

#ifndef DEFAULT_ALIGNMENT
#define DEFAULT_ALIGNMENT (2*sizeof(void *))
#endif

#define Bytes(n)      (n)
#define Kilobytes(n)  (n << 10)
#define Megabytes(n)  (n << 20)
#define Gigabytes(n)  (((U64)n) << 30)
#define Terabytes(n)  (((U64)n) << 40)

#define Thousand(n) ((n)*1000)
#define Million(n)  ((n)*1000000)
#define Billion(n)  ((n)*1000000000LL)

#define ARENA_COMMIT_GRANULARITY Kilobytes(4)
#define ARENA_DECOMMIT_THRESHOLD Megabytes(64)

#define Swap(type, a, b) do{ type _swapper_ = a; a = b; b = _swapper_; }while(0)
#define Min(a, b) (((a)<(b)) ? (a) : (b))
#define Max(a, b) (((a)>(b)) ? (a) : (b))

function U64
OS_PageSize (void)
{
  SYSTEM_INFO info;
  GetSystemInfo (&info);
  return info.dwPageSize;
}

function void *
OS_Reserve (U64 size)
{
  U64 gb_snapped_size = size;
  gb_snapped_size += Gigabytes (1) - 1;
  gb_snapped_size -= gb_snapped_size % Gigabytes (1);
  void *ptr = VirtualAlloc (0, gb_snapped_size, MEM_RESERVE, PAGE_NOACCESS);
  return ptr;
}

function void
OS_Release (void *ptr, U64 size)
{
  VirtualFree (ptr, 0, MEM_RELEASE);
}

function void
OS_Commit (void *ptr, U64 size)
{
  U64 page_snapped_size = size;
  page_snapped_size += OS_PageSize () - 1;
  page_snapped_size -= page_snapped_size % OS_PageSize ();
  VirtualAlloc (ptr, page_snapped_size, MEM_COMMIT, PAGE_READWRITE);
}

function void
OS_Decommit (void *ptr, U64 size)
{
  VirtualFree (ptr, size, MEM_DECOMMIT);
}

function bool
is_power_of_two (UPTR x)
{
  return (x & (x - 1)) == 0;
}

function U64
calc_padding_with_header (UPTR ptr, UPTR alignment, U64 header_size)
{
  UPTR p, a, modulo, padding, needed_space;
  
  assert (is_power_of_two (alignment));
  
  p = ptr;
  a = alignment;
  modulo = p & (a - 1);		// (p % a) as it assumes alignment is a power of two
  
  padding = 0;
  needed_space = 0;
  
  if (modulo != 0)
  {				// Same logic as 'align_forward'
    padding = a - modulo;
  }
  
  needed_space = (UPTR) header_size;
  
  if (padding < needed_space)
  {
    needed_space -= padding;
    
    if ((needed_space & (a - 1)) != 0)
    {
      padding += a * (1 + (needed_space / a));
    } else
    {
      padding += a * (needed_space / a);
    }
  }
  
  return (U64) padding;
}

// Free list style allocator. 
// Ideally we can implement this using a red-black tree.
// We're gonna use a red-black tree b/c fuck you swag money
typedef struct Header Header;
struct Header 
{
  U64 blockSize;
  U64 padding;
};

typedef enum COLOR 
{
  BLACK, RED
}
COLOR;

typedef struct Node Node;
struct Node
{
  Node *parent;
  Node *child[2];
  
  COLOR color;
  U8 key;
};

#define NIL   NULL
#define LEFT  0
#define RIGHT 1
#define left  child[LEFT]
#define right child[RIGHT]

typedef struct RBTree RBTree;
struct RBTree
{
  Node *root;
};



int
main ()
{
  return 0;
}


#if 0
// testing
typedef struct SomeData SomeData;
struct SomeData
{
  char *nameOfData;
  int actualData;
};

#define Alloc(fl, type) (type *)free_list_alloc((fl), (U64)sizeof(type))
#define Dealloc(fl, ptr) free_list_free(fl, (void *)ptr)
int
main (void)
{
  FreeList *memory = free_list_init (Gigabytes (1));
  
  if (memory)
  {
    printf ("oh shit we got some memory to work with of size: %zu.\n",
            memory->size);
    
    SomeData *d1 = Alloc (memory, SomeData);
    SomeData *d2 = Alloc (memory, SomeData);
    SomeData *d3 = Alloc (memory, SomeData);
    SomeData *d4 = Alloc (memory, SomeData);
    SomeData *d5 = Alloc (memory, SomeData);
    
    if (d1)
    {
      printf ("oh shit we got some valid memory back.\n");
      
      d1->nameOfData = "d1";
      d1->actualData = 152329;
      d2->nameOfData = "d2";
      d2->actualData = 1234628;
      d3->nameOfData = "d3";
      d3->actualData = 345235;
      d4->nameOfData = "d4";
      d4->actualData = 253413;
      d5->nameOfData = "d5";
      d5->actualData = 7856781;
      
      printf ("the name of data1: %s.\n", d1->nameOfData);
      printf ("the data of data1: %d.\n", d1->actualData);
      printf ("the name of data2: %s.\n", d2->nameOfData);
      printf ("the data of data2: %d.\n", d2->actualData);
      printf ("the name of data3: %s.\n", d3->nameOfData);
      printf ("the data of data3: %d.\n", d3->actualData);
      printf ("the name of data4: %s.\n", d4->nameOfData);
      printf ("the data of data4: %d.\n", d4->actualData);
      printf ("the name of data5: %s.\n", d5->nameOfData);
      printf ("the data of data5: %d.\n", d5->actualData);
      
      printf ("current amount of memory used: %zu.\n", memory->used);
      printf ("current amount of memory left: %zu.\n",
              memory->size - memory->used);
      
      Dealloc (memory, d3);
      Dealloc (memory, d5);
      Dealloc (memory, d4);
      
      printf ("d3 and d5 AND d4 dropped.\n");
      printf ("current amount of memory used: %zu.\n", memory->used);
      printf ("current amount of memory left: %zu.\n",
              memory->size - memory->used);
    }
    
  }
  
  return 0;
}
#endif 