#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <stdint.h>
#include <windows.h>
#include <windowsx.h>
#include <tlhelp32.h>
#include <Shlobj.h>
#include <direct.h>
#include <stdbool.h>
#include <tchar.h>
#include <strsafe.h>
#include <fileapi.h>
#include <math.h>
#include <time.h>
#include <assert.h>
#include <inttypes.h>

// allocator based on https://www.gingerbill.org/article/2019/02/08/memory-allocation-strategies-002/
// love u bill <3

#define function   static
#define MemoryCopy memcpy
#define MemorySet  memset

typedef uint8_t U8;
typedef uint64_t U64;
typedef uintptr_t UPTR;

typedef struct Arena Arena;
struct Arena
{
  U64 *buffer;
  U64 remaining;
  U64 prevOffset;
  U64 currOffset;
  U64 used;
};

#ifndef DEFAULT_ALIGNMENT
#define DEFAULT_ALIGNMENT (2*sizeof(void *))
#endif

#define Bytes(n)      (n)
#define Kilobytes(n)  (n << 10)
#define Megabytes(n)  (n << 20)
#define Gigabytes(n)  (((U64)n) << 30)
#define Terabytes(n)  (((U64)n) << 40)

#define Thousand(n) ((n)*1000)
#define Million(n)  ((n)*1000000)
#define Billion(n)  ((n)*1000000000LL)

#define ARENA_COMMIT_GRANULARITY Kilobytes(4)
#define ARENA_DECOMMIT_THRESHOLD Megabytes(64)

#define Swap(type, a, b) do{ type _swapper_ = a; a = b; b = _swapper_; }while(0)
#define Min(a, b) (((a)<(b)) ? (a) : (b))
#define Max(a, b) (((a)>(b)) ? (a) : (b))
/*
* GBill: On virtually all architectures, 
* the amount of bytes that something must be 
* aligned by must be a power of two (1, 2, 4, 8, 16, etc).
*/
function bool
is_power_of_two (uintptr_t x)
{
  return (x & (x - 1)) == 0;
}

function UPTR
align_forward (UPTR ptr, U8 align)
{
  UPTR p, a, mod;

  assert (is_power_of_two (align));

  p = ptr;
  a = (UPTR) align;

  mod = p & (a - 1);		// GBill: faster than (p % a) since a is gaurenteed to be a power of 2

  // if p is NOT aligned then we push the ptr addr to the next value
  if (mod != 0)
  {
    p += a - mod;
  }

  return p;
}

function U64
OS_PageSize (void)
{
  SYSTEM_INFO info;
  GetSystemInfo (&info);
  return info.dwPageSize;
}

function void *
OS_Reserve (U64 size)
{
  U64 gb_snapped_size = size;
  gb_snapped_size += Gigabytes (1) - 1;
  gb_snapped_size -= gb_snapped_size % Gigabytes (1);
  void *ptr = VirtualAlloc (0, gb_snapped_size, MEM_RESERVE, PAGE_NOACCESS);
  return ptr;
}

function void
OS_Release (void *ptr, U64 size)
{
  VirtualFree (ptr, 0, MEM_RELEASE);
}

function void
OS_Commit (void *ptr, U64 size)
{
  U64 page_snapped_size = size;
  page_snapped_size += OS_PageSize () - 1;
  page_snapped_size -= page_snapped_size % OS_PageSize ();
  VirtualAlloc (ptr, page_snapped_size, MEM_COMMIT, PAGE_READWRITE);
}

function void
OS_Decommit (void *ptr, U64 size)
{
  VirtualFree (ptr, size, MEM_DECOMMIT);
}

// JUICE [start]
function void *
arena_alloc_align (Arena * a, U64 size, U8 align_)
{
  void *result = 0;
  if (a->prevOffset + size <= a->remaining)
  {
    U8 *base = (U8 *) a;
    U64 postAlignPos = (a->currOffset + (align_ - 1));
    postAlignPos -= postAlignPos % align_;
    U64 align = postAlignPos - a->currOffset;
    result = base + a->currOffset + align;
    a->currOffset += size + align;
    if (a->prevOffset < a->currOffset)
    {
      U64 sizeToCommit = a->currOffset - a->prevOffset;
      sizeToCommit += ARENA_COMMIT_GRANULARITY - 1;
      sizeToCommit -= sizeToCommit % ARENA_COMMIT_GRANULARITY;
      OS_Commit (base + a->prevOffset, sizeToCommit);
      a->prevOffset += sizeToCommit;
      a->used += sizeToCommit;
      a->remaining -= a->used;
    }
  } else
  {
    // NOTE(rjf): fallback strategy. right now, just fail.
  }
  return result;
}

function Arena *
arena_init (U64 size)
{
  U64 size_roundup_granularity = Megabytes (64);
  size += size_roundup_granularity - 1;
  size -= size % size_roundup_granularity;
  void *block = OS_Reserve (size);
  U64 initial_commit_size = ARENA_COMMIT_GRANULARITY;
  assert (initial_commit_size >= sizeof (Arena));
  OS_Commit (block, initial_commit_size);
  Arena *arena = (Arena *) block;
  arena->currOffset = sizeof (Arena);
  arena->prevOffset = 0;
  arena->remaining = size;
  arena->used = 0;
  return arena;
}

function void *
arena_alloc (Arena * a, U64 size)
{
  return arena_alloc_align (a, size, DEFAULT_ALIGNMENT);
}

function void
arena_free_all (Arena * a)
{
  a->currOffset = 0;
  a->prevOffset = 0;
}

function void
arena_remove_to (Arena * a, U64 pos)
{
  U64 minPos = sizeof (Arena);
  U64 newPos = Max (minPos, pos);
  a->prevOffset = newPos;
  U64 posAlignedToCommitChunks = a->prevOffset + ARENA_COMMIT_GRANULARITY - 1;
  posAlignedToCommitChunks -=
    posAlignedToCommitChunks % ARENA_COMMIT_GRANULARITY;

  if (posAlignedToCommitChunks + ARENA_DECOMMIT_THRESHOLD <= a->currOffset)
  {
    U8 *base = (U8 *) a;
    U64 sizeToRemove = a->currOffset - posAlignedToCommitChunks;
    OS_Decommit (base + posAlignedToCommitChunks, sizeToRemove);
    printf (" size to remove %" PRIu64 "\n", sizeToRemove);
    a->used -= sizeToRemove;
    a->remaining += a->used;
  } else
  {
    printf ("not removing to?\n");
  }
}

function void
arena_remove (Arena * a, U64 size)
{
  U64 minPos = sizeof (Arena);
  U64 sizeToRemove = Min (size, a->prevOffset);
  U64 newPos = a->prevOffset - sizeToRemove;
  newPos = Max (newPos, minPos);
  arena_remove_to (a, newPos);
}

#define Alloc(arena, type) (type *)arena_alloc((arena), (U64)sizeof(type))
#define ArenaInit(size)            arena_init((size))
#define ArenaFreeAll(arena)        arena_free_all((arena))
#define Dealloc(arena, type)       arena_remove((arena), (U64)sizeof(type))

typedef struct SomeData SomeData;
struct SomeData
{
  int i;
  U8 *string;
};

int
main (void)
{
  Arena *arena = arena_init (Gigabytes (1));
  if (arena)
  {
    printf ("swag money. you have this much memory left in the arena: %"
	    PRIu64 "\n", arena->remaining);
    SomeData *d1 = Alloc (arena, SomeData);
    d1->i = 8173123;
    d1->string = "Poo poo pee pee";
    printf ("used:      %" PRIu64 "\n", arena->used);
    printf ("remaining: %" PRIu64 "\n", arena->remaining);
    printf ("d1-> %d\n", d1->i);
    printf ("d1-> %s\n", d1->string);
    Dealloc (arena, SomeData);
    printf ("used (post dealloc of d1):      %" PRIu64 "\n", arena->used);
    printf ("remaining(post dealloc of d1):  %" PRIu64 "\n",
	    arena->remaining);
    printf ("d1-> %d\n", d1->i);
    printf ("d1-> %s\n", d1->string);
  } else
  {
    printf ("fuck we failed... WE DIDN'T LISTEN!");
  }
  return 0;
}

// JUICE [end]
